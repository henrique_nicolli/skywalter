package com.skywalker;

import com.skywalker.dto.MovieLetterMetricsDTO;
import com.skywalker.model.Movie;
import com.skywalker.resource.MovieResource;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MovieResourceTests {

    @Autowired
    private MovieResource movieResource;

    private static Movie movie;

    @BeforeClass
    public static void initMovie(){
        movie = new Movie();
        movie.setTitle("qqqqq wwww rrr");
        movie.setUser_rating(10);
        movie.setRelease_date(LocalDateTime.now());
        movie.setSynopsis("Great film");
    }

    @Test
    public void a_createMovieTest(){
        ResponseEntity<Movie> response = movieResource.create(movie);
        movie = response.getBody();
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void b_findByMovieIdTest(){
        ResponseEntity<Movie> response = movieResource.findById(movie.getId());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void c_updateMovieRatingTest(){
        int newUserRating = ThreadLocalRandom.current().nextInt(0, 10 + 1);

        movie.setUser_rating(newUserRating);
        ResponseEntity<Movie> response = movieResource.updateMovieRating(movie.getId(), movie);

        assertEquals(newUserRating, response.getBody().getUser_rating());
    }

    @Test
    public void c_updateMovieRatingOutIntervalTest(){
        int newUserRating = ThreadLocalRandom.current().nextInt(11, 1000 + 1);

        movie.setUser_rating(newUserRating);
        ResponseEntity<Movie> response = movieResource.updateMovieRating(movie.getId(), movie);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    public void d_updateMovieRatingNegativeNumberTest(){
        int newUserRating = ThreadLocalRandom.current().nextInt(-100, 0 + 1);

        movie.setUser_rating(newUserRating);
        ResponseEntity<Movie> response = movieResource.updateMovieRating(movie.getId(), movie);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    public void e_getMovieLetterMetricsTest(){
        ResponseEntity<List<MovieLetterMetricsDTO>> movieLetterMetricsDTOList = movieResource.getLetterMetricsTop10();
        MovieLetterMetricsDTO movieLetterMetricsDTO = movieLetterMetricsDTOList.getBody().get(0);

        assertEquals("q", movieLetterMetricsDTO.getLetter());
        assertEquals(5, movieLetterMetricsDTO.getAmount());
    }

    @Test
    public void f_getMovieLetterMetricsTest(){
        ResponseEntity<List<MovieLetterMetricsDTO>> movieLetterMetricsDTOList = movieResource.getLetterMetricsTop10();
        MovieLetterMetricsDTO movieLetterMetricsDTO = movieLetterMetricsDTOList.getBody().get(1);

        assertEquals("w", movieLetterMetricsDTO.getLetter());
        assertEquals(4, movieLetterMetricsDTO.getAmount());
    }

    @Test
    public void g_getMovieLetterMetricsTest(){
        ResponseEntity<List<MovieLetterMetricsDTO>> movieLetterMetricsDTOList = movieResource.getLetterMetricsTop10();
        MovieLetterMetricsDTO movieLetterMetricsDTO = movieLetterMetricsDTOList.getBody().get(2);

        assertEquals("r", movieLetterMetricsDTO.getLetter());
        assertEquals(3, movieLetterMetricsDTO.getAmount());
    }

    @Test
    public void h_clearData(){
        movieResource.clear(movie.getId());
    }


}

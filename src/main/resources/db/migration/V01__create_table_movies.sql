CREATE TABLE IF NOT EXISTS movie (
    id SERIAL PRIMARY KEY,
    title VARCHAR(30),
    release_date TIMESTAMP,
    synopsis TEXT,
    user_rating INTEGER NOT NULL
);














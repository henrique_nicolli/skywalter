package com.skywalker.util;

import com.skywalker.dto.MovieLetterMetricsDTO;
import com.skywalker.model.Movie;

import java.util.*;
import java.util.stream.Collectors;

public class MovieUtils {

    public static Map<Character, Integer> maximumConsonantOccurringChars(String str, Boolean skipSpaces) {
        str = str.replaceAll("[aeiouAEIOU]", "");

        Map<Character, Integer> map = new HashMap<>();
        int maxOccurring = 0;

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);

            if (skipSpaces && ch == ' ')
                continue;

            if (map.containsKey(ch)) {
                map.put(ch, map.get(ch) + 1);
            } else {
                map.put(ch, 1);
            }

            if (map.get(ch) > maxOccurring) {
                maxOccurring = map.get(ch);
            }
        }

        return map;
    }

    public static List<MovieLetterMetricsDTO> orderAndGetTop10(List<MovieLetterMetricsDTO> moviesLetterMetricsVOList){
        moviesLetterMetricsVOList.sort(Comparator.comparing(MovieLetterMetricsDTO::getAmount).reversed());
        moviesLetterMetricsVOList = moviesLetterMetricsVOList.stream().limit(10).collect(Collectors.toList());
        return moviesLetterMetricsVOList;
    }

    public static String getAllMoviesTitle(List<Movie> allMovies) {
        String allMoviesTitleString = "";
        for(Movie movie : allMovies) {
            allMoviesTitleString += movie.getTitle();
        }
        return allMoviesTitleString;
    }
}

package com.skywalker.dto;

public class MovieLetterMetricsDTO {

    private String letter;

    private int amount;

    public MovieLetterMetricsDTO() {
    }

    public MovieLetterMetricsDTO(String letter, int amount) {
        this.letter = letter;
        this.amount = amount;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}

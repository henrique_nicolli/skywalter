package com.skywalker.resource;

import com.skywalker.model.Movie;
import com.skywalker.dto.MovieLetterMetricsDTO;
import com.skywalker.repository.MovieRepository;
import com.skywalker.util.MovieUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/movies")
public class MovieResource {

    @Autowired
    private MovieRepository movieRepository;

    @GetMapping
    public ResponseEntity<List<Movie>> findAll(){
        return ResponseEntity.ok().body(movieRepository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> findById(@PathVariable Long id){
        if(movieRepository.findById(id).isPresent()){
            return ResponseEntity.ok().body(movieRepository.findById(id).get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Movie> create(@RequestBody Movie movie){
        return ResponseEntity.status(HttpStatus.CREATED).body(movieRepository.save(movie));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovieRating(@PathVariable Long id, @RequestBody Movie movie){
        Optional<Movie> oldMovie = movieRepository.findById(id);
        if(oldMovie.isPresent()){
            oldMovie.get().setUser_rating(movie.getUser_rating());
            try{
                return ResponseEntity.ok(movieRepository.save(oldMovie.get()));
            } catch (Exception e){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("letter_metrics_top10")
    public ResponseEntity<List<MovieLetterMetricsDTO>> getLetterMetricsTop10(){
        List<Movie> allMovies = movieRepository.findAll();
        List<MovieLetterMetricsDTO> moviesLetterMetricsVOList = new ArrayList<>();

        String allMoviesTitleString = MovieUtils.getAllMoviesTitle(allMovies);

        Map<Character, Integer> map = MovieUtils.maximumConsonantOccurringChars(allMoviesTitleString, true);
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            moviesLetterMetricsVOList.add(new MovieLetterMetricsDTO(String.valueOf(entry.getKey()), entry.getValue()));
        }

        moviesLetterMetricsVOList = MovieUtils.orderAndGetTop10(moviesLetterMetricsVOList);

        return ResponseEntity.ok(moviesLetterMetricsVOList);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Movie> clear(@PathVariable long id){
        movieRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }


}
